import React from 'react';
import ReactDOM from 'react-dom';
import Todo from './Todo/index'
import { Provider } from 'react-redux'
import reducer from './store/reducers/index'
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from "redux-saga";
import { addEvent } from "./store/sagas/saga"
const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(addEvent);
const app = (
  <Provider store={store}>
    <Todo />
  </Provider>
);
ReactDOM.render(app, document.getElementById('root'));


// import React, { Component } from 'react'
// import ReactDOM from 'react-dom'
// import MaterialTable from 'material-table'

// class App extends Component {
//   render() {
//     return (
//       <div style={{ maxWidth: '100%' }}>
//         <MaterialTable
//           options={{
//             sorting: true
//           }}
//           columns={[
//             { title: 'Adı', field: 'name' },
//             { title: 'Soyadı', field: 'surname' },
//             { title: 'Doğum Yılı', field: 'birthYear', type: 'numeric' },
//             { title: 'Doğum Yeri', field: 'birthCity', lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' } }
//           ]}
//           data={[
//           ]}
//           title="Demo Title"
//         />
//       </div>
//     )
//   }
// }

// ReactDOM.render(<App />, document.getElementById('root'));