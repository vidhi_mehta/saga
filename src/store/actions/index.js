import * as actionTypes from './actionTypes'

export const addItem = (title) => {
    return {
        type: actionTypes.ADD_ITEM,
        title: title
    }
}
export const deleteItem = (item) => {
    console.log("i am delete item", item)
    return {
        type: actionTypes.DELETE_ITEM,
        item: item
    }
}
export const editItem = (item) => {
    console.log("i am in edit item", item)
    return {
        type: actionTypes.EDIT_ITEM,
        item: item
    }
}
export const setTitle = (title) => {
    return {
        type: actionTypes.SET_TITLE,
        title: title
    }
}
export const setError = (error) => {
    return {
        type: actionTypes.SET_ERROR,
        error: error
    }
}
export const setItem = (item) => {
    return {
        type: actionTypes.SET_ITEM,
        item: item
    }
}
export const setEdit = () => {
    return {
        type: actionTypes.SET_EDIT
    }
}
export const apiData = () => {
    console.log("in action type api data")
    return {
        type: actionTypes.API_DATA
    }
}