import { takeLatest, put, call } from "redux-saga/effects";
import axios from 'axios';

function* addItemAsync(props) {
    console.log("in add saga", props.title)
    yield put({ type: "ADD_ITEM_A", payload: props.title });
}
function* editItemAsync(props) {
    console.log("in edit saga", props.item)
    yield put({ type: "EDIT_ITEM_A", payload: props.item });
}
function* deleteItemAsync(props) {
    console.log("in delete saga", props.item)
    yield put({ type: "DELETE_ITEM_A", payload: props.item });
}
function* apiDataAsync(props) {
    console.log("in api saga", props)
    // const apiResponse = yield fetch('https://jsonplaceholder.typicode.com/todos')
    //     .then(response => response.json());
    // console.log("const", apiResponse)
    // yield put({ type: "API_DATA_A", payload: apiResponse });
    try {
        console.log("Calling API");
        const response = yield call(
            axios.get,
            "https://jsonplaceholder.typicode.com/todos"
        );
        const apiResponse = response.data

        yield put({ type: "API_DATA_A", payload: apiResponse });
    } catch (error) {
        console.log("error", error);
    }
}
export function* addEvent() {
    yield takeLatest("ADD_ITEM", addItemAsync);
    yield takeLatest("EDIT_ITEM", editItemAsync);
    yield takeLatest("DELETE_ITEM", deleteItemAsync);
    yield takeLatest("API_DATA", apiDataAsync);
}

