
import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import * as actionTypes from '../store/actions';
import MaterialTable from 'material-table'

const useStyles = makeStyles({
    container: {
        padding: 16
    }
});

function TodoList({ todoList, setTitle, setItem, setEdit, deleteItem }) {
    const { useState } = React;
    const [selectedRow, setSelectedRow] = useState(null);

    const classes = useStyles();
    const handleEdit = (item) => {
        setTitle(item.value);
        setEdit();
        setItem(item);
    }

    const handleDelete = (item) => {
        setItem(item);
        deleteItem(item);
    }
    function searchInData(e) {
    }
    return (
        <Container className={classes.container} maxWidth="md">
            <div style={{ maxWidth: '100%' }}>
                {console.log("hi", todoList)}
                <div>
                    <MaterialTable
                        onSearchChange={e => searchInData(e)}
                        columns={[
                            { title: 'ID', field: 'id' },
                            {
                                title: 'Event Name', field: 'value',
                                customSort: (a, b) => a.value.length - b.value.length

                            },
                        ]}
                        data={todoList}
                        options={{
                            sorting: true,
                            search: true,
                            rowStyle: rowData => ({
                                backgroundColor: (selectedRow === rowData.tableData.id) ? '#EEE' : '#FFF'
                            })
                        }}
                        onRowClick={((evt, selectedRow) => setSelectedRow(selectedRow.tableData.id))}
                        title="Demo Title"
                        actions={[
                            {
                                icon: 'edit',
                                tooltip: 'Edit Event',
                                onClick: (event, rowData) => {
                                    console.log("i am id", rowData.id);
                                    handleEdit(rowData)
                                }
                            },
                            {
                                icon: 'delete',
                                tooltip: 'Delete Event',
                                onClick: (event, rowData) => {
                                    console.log("row", rowData)
                                    console.log("You want to delete " + rowData.value)
                                    handleDelete(rowData)
                                }
                            }
                        ]}
                    />

                </div>
            </div>
        </Container>
    )

}
const mapStateToProps = (state) => {
    return {
        todoList: state.items
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setTitle: (title) => dispatch(actionTypes.setTitle(title)),
        setItem: (item) => dispatch(actionTypes.setItem(item)),
        deleteItem: (item) => dispatch(actionTypes.deleteItem(item)),
        setEdit: () => dispatch(actionTypes.setEdit()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);